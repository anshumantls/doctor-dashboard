export const SUBMIT_DATA = 'SUBMIT_DATA';
export const SUBMIT_TIMINGS = 'SUBMIT_TIMINGS';
export const ADD_DOCTOR = 'ADD_DOCTOR';
export const FETCH_DATA = 'FETCH_DATA';