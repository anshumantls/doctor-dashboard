import * as actionTypes from './action-types';

export const submitDoctorData = (value) => {
    return {
        type: actionTypes.SUBMIT_DATA,
        payload: value
    }
}

export const submitDoctorSchedule = (value) => {
    return {
        type: actionTypes.SUBMIT_TIMINGS,
        payload: value
    }
} 

export const addNewDoctor = (value) => {
    return {
        type: actionTypes.ADD_DOCTOR,
        payload: value
    }
}