import { FETCH_DATA } from './action-types';
import { getDoctorListFromApi } from '../../http/http-service';

export const _dispatchFetchData = (data) => {
    return {
        type: FETCH_DATA,
        payload: data.doctors
    }
}

export const fetchApiData = (value) => {
   return (dispatch, getState) => {
       return new Promise(async(resolve, reject) => {
           try {
               const response = await getDoctorListFromApi(value);
               if(response) {
                   resolve(
                       dispatch(_dispatchFetchData(response))
                   );
               } else {
                   resolve();
               }
           } catch(error) {
                console.log(error)
                reject(error);
           }
       })
   }
}