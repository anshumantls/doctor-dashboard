import * as actionTypes from '../actions/action-types';

const initialState = {
    doctorDetails: {},
    doctorTimingDetails: [],
    doctors:[],
    fetchedDoctor: []
}

export const reducer = (state=initialState, actions) => {
    switch (actions.type) {
        case actionTypes.SUBMIT_DATA:
            console.log(actions.payload);
            console.log(state)
            return {
                ...state,
                doctorDetails: actions.payload
            }
        case actionTypes.ADD_DOCTOR:
            const newDoctor = {
                doctorDetails: state.doctorDetails,
                avilability: actions.payload
            }
            console.log(state);
            return {
                ...state,
                doctors: state.doctors.concat(newDoctor),
                doctorDetails: {},
                doctorTimingDetails: []
            }
        case actionTypes.FETCH_DATA:
            return {
                ...state,
                fetchedDoctor:actions.payload
            }
        default:
            return state;
    }            
};