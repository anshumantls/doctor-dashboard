import React, { Component } from 'react'
import { makeGetRequest } from '../http/http-service';
import { connect } from 'react-redux';
import { submitDoctorData } from '../redux/actions/add-doctor-action-creator';
import { Redirect } from 'react-router-dom';

class EditDoctorDetails extends Component {
    state = {
        doctorDetails:{
            fullName:'',
            speciality:'',
            experience:'',
            fee:'',
            qualification:'',
            clinicOrHospitalName:'',
            languages:[],
            email:'',
            phone:'',
            gender:'',
            registrationNumber:'',
            graduation:'',
            specialty:'',
            superSpeciality:''
        },
        isDirty:{
            fullName:false,
            speciality:false,
            experience:false,
            fee:false,
            qualification:false,
            clinicOrHospitalName:false,
            languages:false,
            email:false,
            phone:false,
            gender:false,
            registrationNumber:false,
            graduation:false,
            specialty:false,
            superSpeciality:false
        },
        errors:{},
        saveError:false,
        specialties: '',
        languages: ['Hindi','English','Telugu','Gujrati','Tamil','Bengali','Oriya','Assamese','Marathi','Punjabi','Malyalam','Kannada'],
        redirect:false
    }

    componentDidMount () {
        makeGetRequest('http://178.128.127.115:3000/admin/v1/specialties')
        .then(data => this.setState({specialties:data.specialties},() => {console.log(this.state.specialties)}));
    }

    _validateForm = () => {
        const { doctorDetails, errors, isDirty } = this.state;
        Object.keys(doctorDetails).forEach((each) => {
          if (each === "email" && isDirty.email) {
            if (!doctorDetails.email.trim().length) {
              errors.email = "*Required ";
            } else if (
              doctorDetails.email.trim().length &&
              !new RegExp(
                "^[a-zA-Z0-9]{1}[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,3}$"
              ).test(doctorDetails.email)
            ) {
              errors.email = "Invalid Email ";
            } else {
              delete errors[each];
              isDirty.email = false;
            }
          }
          else if (each === 'fullName' && isDirty.fullName) {
              if(!doctorDetails.fullName.trim().length) {
                  errors.fullName = "*Required ";
              } else if (doctorDetails.fullName.trim().length < 3) {
                errors.fullName = 'Name must be minimum 3 letters long '
              } else {
                  delete errors[each];
                  isDirty.fullName = false;
              }
          }
          else if (each === 'experience' && isDirty.experience) {
              if(!doctorDetails.experience.trim().length) {
                  errors.experience = "*Required";
              } else if (doctorDetails.experience < 0) {
                  errors.experience = "Experience can't be negative"
              } else {
                  delete errors[each];
                  isDirty.experience = false;
              }
          }
          else if (each === 'fee' && isDirty.fee) {
              if(!doctorDetails.fee.trim().length) {
                  errors.fee = "*Required";
              } else {
                  delete errors[each];
                  isDirty.fee = false;
              }
          }
          else if (each === 'qualification' && isDirty.qualification) {
              if(!doctorDetails.qualification.trim().length) {
                  errors.qualification = "*Required";
              } else {
                  delete errors[each];
                  isDirty.qualification = false;
              }
          }
          else if (each === 'clinicOrHospitalName' && isDirty.clinicOrHospitalName) {
              if(!doctorDetails.clinicOrHospitalName.trim().length) {
                  errors.clinicOrHospitalName = "*Required";
              } else {
                  delete errors[each];
                  isDirty.clinicOrHospitalName = false;
              }
          }
          else if (each === 'languages' && isDirty.languages) {
              if (!doctorDetails.languages.length) {
                  errors.languages = "At least one language is required";
              } else {
                  delete errors[each];
                  isDirty.languages = false;
              }
          }
          else if (each === 'phone' && isDirty.phone) {
              if (!doctorDetails.phone.trim().length) {
                  errors.phone = "*Required"
              } else if (doctorDetails.phone.trim().length < 10
                        || doctorDetails.phone.trim().length > 10) {
                  errors.phone = "Phone must be 10 digits"
              } else {
                  delete errors[each];
                  isDirty.phone = false;
              }
          }
          else if (each === 'registrationNumber' && isDirty.registrationNumber) {
              if (!doctorDetails.registrationNumber.trim().length) {
                  errors.registrationNumber = "*Required";
              } else {
                  delete errors[each];
                  isDirty.registrationNumber = false;
              }
          }
          else if (each === 'registrationNumber' && isDirty.registrationNumber) {
              if (!doctorDetails.registrationNumber.trim().length) {
                  errors.registrationNumber = "*Required";
              } else {
                  delete errors[each];
                  isDirty.registrationNumber = false;
              }
          }
          else if (each === 'specialty' && isDirty.specialty) {
              if (!doctorDetails.specialty.trim().length) {
                  errors.specialty = "*Required";
              } else {
                  delete errors[each];
                  isDirty.specialty = false;
              }
          }
          else if (each === 'graduation' && isDirty.graduation) {
              if (!doctorDetails.graduation.trim().length) {
                  errors.graduation = "*Required";
              } else {
                  delete errors[each];
                  isDirty.graduation = false;
              }
          }
          else if (each === 'superSpeciality' && isDirty.superSpeciality) {
              if (!doctorDetails.superSpeciality.trim().length) {
                  errors.superSpeciality = "*Required";
              } else {
                  delete errors[each];
                  isDirty.superSpeciality = false;
              }
          }

        });
        this.setState({ errors });
        return Object.keys(errors).length ? errors : null;
    }

    _handleOnChange = (field, value) => {
        const { doctorDetails, isDirty } = this.state;
        doctorDetails[field] = value;
        isDirty[field] = true;
        this.setState({doctorDetails, isDirty},() => this._consoleAndValidation())
    }

    _consoleAndValidation = () => {
        this._validateForm()
    }
    
    _languageChecked = ( language ) => {//for checking if the language needs to be checked or not while rendering
        let passOrNot = this.state.doctorDetails.languages.includes(language)
        return passOrNot;
    }

    _chooseLanguage = (language) => {//checking and unchecking language 
        let itHave = this.state.doctorDetails.languages.includes(language)
        console.log(itHave);
        if(!itHave){
            const newLanguages = [...this.state.doctorDetails.languages]
            newLanguages.push(language);
            this._handleOnChange('languages',newLanguages);
        }
        else {
            const newLanguages = [...this.state.doctorDetails.languages]
            const index = newLanguages.indexOf(language);
            newLanguages.splice(index,1);
            this._handleOnChange('languages',newLanguages);
        }
    }

    _submitHandler = () => {
        let checkEmpty = false;
        Object.keys(this.state.doctorDetails).forEach(each => {
            if(!this.state.doctorDetails[each].length) {
                checkEmpty = true;
            }
        })
        if (!Object.keys(this.state.errors).length && !checkEmpty) {
            this.setState({saveError:false});
            let payload = this.state.doctorDetails;
            payload['id'] = Date.now();
            this.props.saveDoctorDetails(payload);
            this.setState({redirect:true});
            console.log(this.state.doctorDetails);
        } else {
            this.setState({saveError:true});
        }
    }

    render () {
        // const {errors} = this.state
        let specialityList = ''
        if(this.state.specialties) {
            specialityList = this.state.specialties.map(speciality => {
                return <option key={speciality.id} value={speciality.id}>{speciality.name}</option>
            });
        }
        let languagesList =''
        if (this.state.languages) {
            languagesList = this.state.languages.slice(0,8).map(language => {
                return (
                    <div key={language}>
                        <label>{language}</label>
                        <input 
                            type='checkbox' 
                            onChange={() => this._chooseLanguage(language)}
                            checked={this._languageChecked(language)}/>
                    </div>
                )
            })
        }
        return (
            <div className='details'>
                <section className='heading'>
                    <p>Edit Basic Info</p>
                </section>

                <section>
                    <label>Name</label>
                    <input 
                        type='text' 
                        minLength='3' 
                        value={this.state.doctorDetails.fullName} 
                        onChange={(event) =>
                              this._handleOnChange("fullName", event.target.value)
                        }/>
                    {this.state.isDirty.fullName ? <p className='error'>{this.state.errors['fullName']}</p> : null}
                    <label>speciality</label>
                    <select 
                        value={this.state.doctorDetails.speciality}
                        onChange={(event) =>
                            this._handleOnChange("speciality", event.target.value)
                      }>
                        {specialityList}
                    </select>
                </section>

                <section>
                    <label>Experience</label>
                    <input 
                        type='number' 
                        minLength='1' 
                        min='0'
                        value={this.state.doctorDetails.experience}
                        onChange={(event) =>
                              this._handleOnChange("experience", event.target.value)
                        }/>
                    {this.state.isDirty.experience ? <p className='error'>{this.state.errors['experience']}</p> : null}
                    <label>Conselt Fees</label>
                    <input 
                        type='number' 
                        min='0'
                        value={this.state.doctorDetails.fee} 
                        onChange={(event) =>
                            this._handleOnChange("fee", event.target.value)
                      }/>
                      {this.state.isDirty.fee ? <p className='error'>{this.state.errors['fee']}</p> : null}
                </section>

                <section>
                    <label>Qualification</label>
                    <input 
                        type='text' 
                        value={this.state.doctorDetails.qualification}
                        onChange={(event) =>
                            this._handleOnChange("qualification", event.target.value)
                      }/>
                      {this.state.isDirty.qualification ? <p className='error'>{this.state.errors['qualification']}</p> : null}
                    <label>Practising At</label>
                    <input type='text' value={this.state.doctorDetails.clinicOrHospitalName}
                         onChange={(event) =>
                            this._handleOnChange("clinicOrHospitalName", event.target.value)
                      }/>
                    {this.state.isDirty.clinicOrHospitalName ? <p className='error'>{this.state.errors['clinicOrHospitalName']}</p> : null}
                </section>

                <section style={{display:'flex',flexDirection:'row'}}>
                    <label>Languages</label>
                    <div style={{display:'flex',flexDirection:'row',boxSizing:'border-box'}}>
                        {languagesList}
                    </div>
                    {this.state.isDirty.languages ? <p className='error'>{this.state.errors['languages']}</p> : null}
                </section>

                <section>
                    <label>Email</label>
                    <input type='email' minLength='3' value={this.state.doctorDetails.email}
                         onChange={(event) =>
                            this._handleOnChange("email", event.target.value)
                      }/>
                    {this.state.isDirty.email ? <p className='error'>{this.state.errors['email']}</p> : null}
                    <label>Phone</label>
                    <input type='number' minLength='10' maxLength='10' value={this.state.doctorDetails.phone} 
                         onChange={(event) =>
                            this._handleOnChange("phone", event.target.value)
                      }/>
                    {this.state.isDirty.phone ? <p className='error'>{this.state.errors['phone']}</p> : null}
                </section>

                <section>
                    <label>Gender</label>
                    <div>
                        <label>Male
                        <input 
                            type='radio' 
                            name='gender' 
                            checked={this.state.doctorDetails.gender === "Male" ? true : false}
                            onChange={(event) =>
                                this._handleOnChange("gender", "Male")
                            }/>
                        </label>
                        <label>Female
                        <input 
                            type='radio' 
                            name='gender' 
                            checked={this.state.doctorDetails.gender === "Female" ? true : false}
                            onChange={(event) =>
                                this._handleOnChange("gender", "Female")
                            }/>
                        </label>
                    </div>
                    <label>Medical Registration Number</label>
                    <input type='text' value={this.state.doctorDetails.registrationNumber}
                         onChange={(event) =>
                            this._handleOnChange("registrationNumber", event.target.value)
                      }/>
                    {this.state.isDirty.registrationNumber ? <p className='error'>{this.state.errors['registrationNumber']}</p> : null}
                </section>

                <section>
                    <label>Graduation</label>
                    <textarea type='text' minLength='3' value={this.state.doctorDetails.graduation}
                         onChange={(event) =>
                            this._handleOnChange("graduation", event.target.value)
                      }/>
                    {this.state.isDirty.graduation ? <p className='error'>{this.state.errors['graduation']}</p> : null}
                    <label>Speciallization</label>
                    <textarea type='text' minLength='3' value={this.state.doctorDetails.specialty}
                         onChange={(event) =>
                            this._handleOnChange("specialty", event.target.value)
                      }/>
                    {this.state.isDirty.specialty ? <p className='error'>{this.state.errors['specialty']}</p> : null}
                </section>

                <section>
                    <label>Super Speciallization</label>
                    <textarea 
                        type='text' 
                        value={this.state.doctorDetails.superSpeciality}
                        onChange={(event) =>
                            this._handleOnChange("superSpeciality", event.target.value)
                        }/>
                    {this.state.isDirty.superSpeciality ? <p className='error'>{this.state.errors['superSpeciality']}</p> : null}
                </section>
                
                <section>
                    <button 
                        className='save'
                        onClick={() => this._submitHandler()}>
                        Save
                    </button>
                    {this.state.saveError ? <p className='error'>One Or Multiple Fields Are Invalid</p> : null}
                </section>
                {this.state.redirect ? <Redirect to='/add-doctor/step2'/> : null}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveDoctorDetails: (value) => dispatch(submitDoctorData(value))
    }
}

export default connect(null, mapDispatchToProps)(EditDoctorDetails);