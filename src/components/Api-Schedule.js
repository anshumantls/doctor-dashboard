import React, { Component } from 'react';
import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

class ApiSchedule extends Component {
    

    _setDayData = (array) => {
        const day = array.map((each, index) => {
            return  <span key={each.id}>
                        {each.from} to {each.to}
                        {array[index+1] ? ' & ' : null}
                    </span>
        });
        return day;
    }

    render() {
        const {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday} = this.props.data
        let monday= '',tuesday='',wednesday='',thursday='',friday='',saturday='',sunday='';
        if(Monday.length) {
            monday = this._setDayData(Monday);
        }

        if(Tuesday.length) {
            tuesday = this._setDayData(Tuesday);
        }

        if(Wednesday.length) {
            wednesday = this._setDayData(Wednesday);
        }

        if(Thursday.length) {
            thursday = this._setDayData(Thursday);
        }

        if(Friday.length) {
            friday = this._setDayData(Friday);
        }

        if(Saturday.length) {
            saturday = this._setDayData(Saturday);
        }

        if(Sunday.length) {
            sunday = this._setDayData(Sunday);
        }
        
        return(
            <UncontrolledPopover trigger="focus" placement="bottom" target={'_lol'+this.props.id}>
                <PopoverHeader>Schedule</PopoverHeader>
                <PopoverBody>
                    {monday.length ? <p>Monday: {monday}</p> : null}  
                    {tuesday.length ? <p>Tuesday: {tuesday}</p> : null}  
                    {wednesday.length ? <p>Wednesday: {wednesday}</p> : null}  
                    {thursday.length ? <p>Thursday: {thursday}</p> : null}  
                    {friday.length ? <p>Friday: {friday}</p> : null}  
                    {saturday.length ? <p>Saturday: {saturday}</p> : null}  
                    {sunday.length ? <p>Sunday: {sunday}</p> : null}                                
                </PopoverBody>
            </UncontrolledPopover>
        );
    }
}

export default ApiSchedule;