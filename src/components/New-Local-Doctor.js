import React, { Component } from 'react';
import { connect } from 'react-redux';
import BootstrapTable from 'react-bootstrap-table-next';
import { Button } from 'reactstrap';

//components
import Schedule from './Schedule';

//images
import EyeIcon from '../assets/eye.png'

class NewLocalDoctor extends Component {

    _consults = () => {
        return '0';
    }

    _status = () => {
        return 'InActive';
    }

    _schedule = (cell,row) => {
        if(cell){
            return  <React.Fragment>
                        <Button id={'_lol'+row.id} type="button">
                                    <img className='icon' alt='view' src={EyeIcon}/>
                                </Button>
                        <Schedule data={cell} id={row.id}/>
                    </React.Fragment>
        } else {
            return 'No Schedule Added';
        }
    }

    render() {

        let columns = [
            {dataField:'fullName', text:'Name'},
            {dataField:'email', text:'Email'},
            {dataField:'phone', text:'Phone'},
            {dataField:'clinicOrHospitalName', text:'Location'},
            {dataField:'specialty', text:'Speciality'},
            {dataField:'fee', text:'Consult Fees'},
            {dataField:'consults', text:'Consults', formatter:this._consults},
            {dataField:'avilability', text:'Schedule', formatter:this._schedule },
            {dataField:'status', text:'Status', formatter:this._status}

        ]

        let newProducts = this.props.list.map(each => {
            each.doctorDetails.avilability = each.avilability
            return each.doctorDetails
        })

        return(
            <div>
                <BootstrapTable
                    keyField="id"
                    data={newProducts}
                    columns={columns}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    return {
        list : state.reducer.doctors
    }
}

export default connect(mapStateToProps)(NewLocalDoctor);