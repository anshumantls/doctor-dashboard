import React, { Component } from 'react';
import { connect } from 'react-redux';
import BootstrapTable from 'react-bootstrap-table-next';
import { Button } from 'reactstrap';
import { fetchApiData } from '../redux/actions/fetch-doctor-action-creator';
import paginationFactory from 'react-bootstrap-table2-paginator';

//components
import ApiSchedule from './Api-Schedule';

//images
import EyeIcon from '../assets/eye.png'

class NewLocalDoctor extends Component {

    componentDidMount() {
        this.props.fetchData();
    }

    _consults = () => {
        return '0';
    }

    _status = () => {
        return 'InActive';
    }

    _location = (cell,row) => {
        if(cell) {
            console.log(cell)
        }
    }

    _schedule = (cell,row) => {
        const newCell =  {
            Monday:[],
            Tuesday:[],
            Wednesday:[],
            Thursday:[],
            Friday:[],
            Saturday:[],
            Sunday:[]
        }
        if(cell.length){
            cell.forEach(each => {
                const data = {
                    from : each.from > 12 ? each.from - 12 + 'PM' : each.from + 'AM',
                    to : each.to > 12 ? each.to - 12 + 'PM' : each.from + 'AM',
                    id : each._id
                }
                newCell[each.day].push(data)
            })
            return  <React.Fragment>
                        <Button id={'_lol'+row.id} type="button">
                                    <img className='icon' alt='view' src={EyeIcon}/>
                                </Button>
                        <ApiSchedule data={newCell} id={row.id}/>
                    </React.Fragment>
        } else {
            return 'No Schedule Added';
        }
    }

    render() {

        let columns = [
            {dataField:'name', text:'Name'},
            {dataField:'email', text:'Email'},
            {dataField:'phone', text:'Phone'},
            {dataField:'location', text:'Location'},
            {dataField:'speciality', text:'Speciality'},
            {dataField:'fee', text:'Consult Fees'},
            {dataField:'consults', text:'Consults', formatter:this._consults},
            {dataField:'availability', text:'Schedule', formatter:this._schedule },
            {dataField:'status', text:'Status', formatter:this._status}

        ]

        let newProducts = this.props.list.map(each => {
            const obj = {
                id: each.id,
                name : each.name.full.trim() ? each.name.full : '-',
                email : each.email,
                phone : each.phone,
                location : each.location ? each.location.streetAddress+' ' + each.location.city+' ' + each.location.state : '-',
                speciality: each._specialty ? each._specialty.name  : 'None',
                fee: each.fee ? each.fee : '-',
                consult: each.totalAppointment,
                availability: each.availability,
                status : each.isActive ? 'Active' : 'InActive'
            }
            return obj;
        })

        return(
            <div>
                <BootstrapTable
                    keyField="id"
                    data={newProducts}
                    columns={columns}
                    pagination={paginationFactory()}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchData : () => dispatch(fetchApiData())
    }
}

const mapStateToProps = state => {
    return {
        list : state.reducer.fetchedDoctor
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLocalDoctor);