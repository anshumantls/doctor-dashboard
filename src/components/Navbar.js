import React from 'react';
import { NavLink } from 'react-router-dom'

import Logo from '../assets/logo.png';

const navbar = () => {
    let style = {
        color: '#578f7f'
    }
    return (
        <nav className='navbar'>
            <div className='logo'><img alt='logo' src={Logo}/></div>
            <div></div>
            <div>
                <ul className='navbarul'>
                    <li><NavLink exact activeStyle={style} to='/doctor-list'>Home</NavLink></li>
                    <li><NavLink exact activeStyle={style} to='/newly-added-doctors'>New Doctor List</NavLink></li>
                    <li><NavLink exact activeStyle={style} to='/doctor-list2'>Doctor List 2</NavLink></li>
                    <li><NavLink exact activeStyle={style} to='/newly-added-doctors2'>New Doctor List 2</NavLink></li>
                </ul>
            </div>
        </nav>
    );
}

export default navbar;