import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addNewDoctor } from '../redux/actions/add-doctor-action-creator';
import { Redirect } from 'react-router-dom';

class EditDoctorTiming extends Component {
    state = {
        availability:[ ],
        days : {
            Monday: [],
            Tuesday:[],
            Wednesday:[],
            Thursday:[],
            Friday:[],
            Saturday:[],
            Sunday: []
        },
        isDirty:{
            Monday: true,
            Tuesday: true,
            Wednesday: true,
            Thursday: true,
            Friday: true,
            Saturday: true,
            Sunday:  true
        },
        errors:{},
        timeValues:[
            {label:'--:--',value:'-'},
            {label:'08:00 AM',value:'8'},
            {label:'09:00 AM',value:'9'},
            {label:'10:00 AM',value:'10'},
            {label:'11:00 AM',value:'11'},
            {label:'12:00 PM',value:'12'},
            {label:'01:00 PM',value:'13'},
            {label:'02:00 PM',value:'14'},
            {label:'03:00 PM',value:'15'},
            {label:'04:00 PM',value:'16'},
            {label:'05:00 PM',value:'17'},
            {label:'06:00 PM',value:'18'},
            {label:'07:00 PM',value:'19'},
            {label:'08:00 PM',value:'20'},
            {label:'09:00 PM',value:'21'},
            {label:'10:00 PM',value:'22'}
        ],
        saveError:false,
        addDoctor:false,
        redirect: false
    }

    _addValueToTheDay = (whichDay) => {
        const newDays = this.state.days;
        let isDirty = this.state.isDirty
        const newData = {
            day : whichDay,
            from: '-',
            to: '-',
            _id: String(Date.now())
        }
        newDays[whichDay].push(newData)
        isDirty[whichDay] = true
        this.setState({days:newDays,isDirty:isDirty}, () => this._consoleAndValidate());
    }

    _removeValueFromTheDay = (whichDay,id) => {
        let newDays = this.state.days;
        let  isDirty = this.state.isDirty;
        newDays[whichDay] = newDays[whichDay].filter(each => {
            return each._id !== id
        });
        isDirty[whichDay] = true;
        this.setState({days:newDays,isDirty:isDirty}, () => this._consoleAndValidate());
    }

    _handleOnChange = (day,fromOrTo,index,value) => {
        const { days, isDirty } = this.state;
        days[day][index][fromOrTo] = value === '-' ? '-' : Number(value) ;
        isDirty[day] = true;
        this.setState({days, isDirty},() => this._consoleAndValidate());
    }

    _consoleAndValidate  = () => {
        console.log(this.state)
        this._validateForm();
    }

    _checkError = (day,type) => {
        if(type === 'empty') {
            return day.some(each => {
                return each.from === '-' || each.to === '-';
            });
        } else if (type === 'valueError') {
            return day.some(each => {
                return each.from >= each.to
            });
        } else if (type === 'overlapError') {
            let previousValue = 0;
            return day.some(each => {
                if(previousValue <= each.from) {
                    previousValue = each.to
                    return false;
                } else {
                    return true
                }
            });
        }
    }

    _validateForm = () => {
        const {days, isDirty, errors} = this.state;
        Object.keys(days).forEach(each => {
            if(each === 'Monday' && isDirty.Monday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Monday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Monday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Monday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Monday = false
                }
            }
            else if (each === 'Tuesday' && isDirty.Tuesday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Tuesday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Tuesday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Tuesday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Tuesday = false
                }
            }
            else if (each === 'Wednesday' && isDirty.Wednesday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Wednesday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Wednesday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Wednesday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Wednesday = false
                }
            }
            else if (each === 'Thursday' && isDirty.Thursday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Thursday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Thursday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Thursday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Thursday = false
                }
            }
            else if (each === 'Friday' && isDirty.Friday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Friday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Friday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Friday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Friday = false
                }
            }
            else if (each === 'Saturday' && isDirty.Saturday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Saturday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Saturday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Saturday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Saturday = false
                }
            }
            else if (each === 'Sunday' && isDirty.Sunday) {
                const emptyError = this._checkError(days[each],'empty');
                const valueError = this._checkError(days[each],'valueError');
                const overlapError = this._checkError(days[each],'overlapError');
                if (emptyError) {
                    errors.Sunday = "Fields can't Be empty"
                } else if (valueError) {
                    errors.Sunday = "From time must be LESSER than To"
                } else if (overlapError) {
                    errors.Sunday = "Timing must not overlap"
                } else {
                    delete errors[each];
                    isDirty.Sunday = false
                }
            }
        })
        this.setState({errors,isDirty})
        console.log(this.state)
    }

    _saveHandeler = () => {
        if (!Object.keys(this.state.errors).length) {
            this.setState({saveError:false});
            console.log(this.state.days," saved");
            this.props.addDoctor(this.state.days);
            this.setState({redirect:true});
        } else {
            this.setState({saveError:true});
        }
    }

    _sortingFunction = (a,b) => {
            let keyA = a.from,
            keyB = b.from;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
    }

    render () {
        let timeInputOptions = '';
        if (this.state.timeValues) {
            timeInputOptions = this.state.timeValues.map(hour => {
                return <option key={hour.value} value={hour.value}>{hour.label}</option>
            });
        }

        const { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday} = this.state.days;
        let sunday = '',monday = '',tuesday = '',wednesday = '',thursday = '',friday = '',saturday = '';

        if (Monday) {
            monday = Monday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Monday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Monday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Monday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Tuesday) {
            tuesday = Tuesday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Tuesday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Tuesday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Tuesday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Wednesday) {
            wednesday = Wednesday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Wednesday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Wednesday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Wednesday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Thursday) {
            thursday = Thursday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Thursday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Thursday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Thursday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Friday) {
            friday = Friday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Friday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Friday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Friday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Saturday) {
            saturday = Saturday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Saturday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Saturday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Saturday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        if (Sunday) {
            sunday = Sunday.sort(this._sortingFunction).map((each,index) => {
                return (
                    <li key={each._id}>
                        <label>From</label>
                        <select 
                            value={each.from}
                            onChange={(event) => this._handleOnChange('Sunday','from',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <label >To</label>
                        <select 
                            value={each.to}
                            onChange={(event) => this._handleOnChange('Sunday','to',index,event.target.value)}>
                            {timeInputOptions}
                        </select>
                        <button className='delete' onClick={() => this._removeValueFromTheDay('Sunday',each._id)}>Delete</button>
                    </li>
                );
            });
        }

        return (
            <div className="timings">
                <section className='heading'>
                    <p>Edit Work Timings</p>
                </section>
                <div>
                    <section>
                        Monday <button className='add' onClick={() => this._addValueToTheDay('Monday')}>Add</button>
                        <ul className='timeList'>
                            {monday}
                        </ul>
                        {this.state.errors.Monday ? <p className='error'>{this.state.errors.Monday}</p> : null}
                    </section>
                    <section>
                        Tuesday <button className='add' onClick={() => this._addValueToTheDay('Tuesday')}>Add</button>
                        <ul className='timeList'>
                            {tuesday}
                        </ul>
                        {this.state.errors.Tuesday ? <p className='error'>{this.state.errors.Tuesday}</p> : null}
                    </section>
                    <section>
                        Wednesday <button className='add' onClick={() => this._addValueToTheDay('Wednesday')}>Add</button>
                        <ul className='timeList'>
                            {wednesday}
                        </ul>
                        {this.state.errors.Wednesday ? <p className='error'>{this.state.errors.Wednesday}</p> : null}
                    </section>
                    <section>
                        Thursday <button className='add' onClick={() => this._addValueToTheDay('Thursday')}>Add</button>
                        <ul className='timeList'>
                            {thursday}
                        </ul>
                        {this.state.errors.Thursday ? <p className='error'>{this.state.errors.Thursday}</p> : null}
                    </section>
                    <section>
                        Friday <button className='add' onClick={() => this._addValueToTheDay('Friday')}>Add</button>
                        <ul className='timeList'>
                           {friday}
                        </ul>
                        {this.state.errors.Friday ? <p className='error'>{this.state.errors.Friday}</p> : null}
                    </section>
                    <section>
                        Saturday <button className='add' onClick={() => this._addValueToTheDay('Saturday')}>Add</button>
                        <ul className='timeList'>
                            {saturday}
                        </ul>
                        {this.state.errors.Saturday ? <p className='error'>{this.state.errors.Saturday}</p> : null}
                    </section>
                    <section>
                        Sunday <button className='add' onClick={() => this._addValueToTheDay('Sunday')}>Add</button>
                        <ul className='timeList'>
                            {sunday}
                        </ul>
                        {this.state.errors.Sunday ? <p className='error'>{this.state.errors.Sunday}</p> : null}
                    </section>
                </div>
                <div>
                    <button 
                        className='save'
                        onClick={() => this._saveHandeler()}>
                            Save
                    </button>
                    {this.state.saveError ? <p className='error'>One Or Multiple Fields Are Invalid</p> : null}
                </div>
                {this.state.redirect ? <Redirect to='/newly-added-doctors'/> : null}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addDoctor: (value) => dispatch(addNewDoctor(value))
    }
}

export default connect(null, mapDispatchToProps)(EditDoctorTiming);