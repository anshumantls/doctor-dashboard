import React , { Component } from 'react';
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

//images
import EyeIcon from '../assets/eye.png';

class Doctor extends Component {
        state = {
            days : {
                Monday: [],
                Tuesday:[],
                Wednesday:[],
                Thursday:[],
                Friday:[],
                Saturday:[],
                Sunday: []
            },
            newDays:{},
            timeValues:[
                {label:'8 AM',value:'8'},
                {label:'9 AM',value:'9'},
                {label:'10 AM',value:'10'},
                {label:'11 AM',value:'11'},
                {label:'12 PM',value:'12'},
                {label:'1 PM',value:'13'},
                {label:'2 PM',value:'14'},
                {label:'3 PM',value:'15'},
                {label:'4 PM',value:'16'},
                {label:'5 PM',value:'17'},
                {label:'6 PM',value:'18'},
                {label:'7 PM',value:'19'},
                {label:'8 PM',value:'20'},
                {label:'9 PM',value:'21'},
                {label:'10 PM',value:'22'},
                {label:'11 PM',value:'23'}
            ]
        }

    componentDidMount () {
        this._setDaysForDoctor(this.props.doctor.availability);
    }

    _checkAndReturnLabel = (value) => {
        const newValue = String(value)
        let label = '';
        this.state.timeValues.forEach(each => {
            if(each.value === newValue) {
                label = each.label
            }
        });
        return label;
    }

    _setDaysForDoctor = (array) => {
        let newDays = this.state.days;
        if(array.length) {
            array.forEach(each => {
                for (let day in newDays) {
                    if (day === each.day) {
                        const newDayData = {
                            id : each._id,
                            day:each.day,
                            from: this._checkAndReturnLabel(each.from),
                            to: this._checkAndReturnLabel(each.to)
                        }
                        newDays[day].push(newDayData)
                    }
                }
            });
            this.setState({days:newDays});
        }
    }

    _setDayData = (array) => {
        const day = array.map((each, index) => {
            return  <span key={each.id}>
                        {each.from} to {each.to}
                        {array[index+1] ? ' & ' : null}
                    </span>
        });
        return day;
    }

    render() {
        const {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday} = this.state.days
        let monday= '',tuesday='',wednesday='',thursday='',friday='',saturday='',sunday='';
        if(Monday.length) {
            monday = this._setDayData(Monday);
        }

        if(Tuesday.length) {
            tuesday = this._setDayData(Tuesday);
        }

        if(Wednesday.length) {
            wednesday = this._setDayData(Wednesday);
        }

        if(Thursday.length) {
            thursday = this._setDayData(Thursday);
        }

        if(Friday.length) {
            friday = this._setDayData(Friday);
        }

        if(Saturday.length) {
            saturday = this._setDayData(Saturday);
        }

        if(Sunday.length) {
            sunday = this._setDayData(Sunday);
        }
        

        return (
                <tr>
                    <td>{this.props.doctor.name.full}</td>
                    <td>{this.props.doctor.email}</td>
                    <td>{this.props.doctor.phone}</td>
                    {   this.props.doctor.location ? 
                        <td>{this.props.doctor.location.streetAddress ? this.props.doctor.location.streetAddress+',' : null}
                            {this.props.doctor.location.city ? this.props.doctor.location.city+',' : null}
                            {this.props.doctor.location.state ? this.props.doctor.location.state : null }</td> : <td>-</td>}
                    <td>{this.props.doctor._specialty ? this.props.doctor._specialty.name : '-'}</td>
                    <td>{this.props.doctor.fee ? this.props.doctor.fee : '-'}</td>
                    <td>{this.props.doctor.totalAppointment}</td>
                    <td>
                        <Button id={'_lol'+this.props.doctor.id} type="button">
                            <img className='icon' alt='view' src={EyeIcon}/>
                        </Button>
                        <UncontrolledPopover trigger="focus" placement="bottom" target={'_lol'+this.props.doctor.id}>
                            <PopoverHeader>Schedule</PopoverHeader>
                            <PopoverBody>
                                {monday.length ? <p>Monday: {monday}</p> : null}  
                                {tuesday.length ? <p>Tuesday: {tuesday}</p> : null}  
                                {wednesday.length ? <p>Wednesday: {wednesday}</p> : null}  
                                {thursday.length ? <p>Thursday: {thursday}</p> : null}  
                                {friday.length ? <p>Friday: {friday}</p> : null}  
                                {saturday.length ? <p>Saturday: {saturday}</p> : null}  
                                {sunday.length ? <p>Sunday: {sunday}</p> : null}                                
                            </PopoverBody>
                        </UncontrolledPopover>
                    </td>
                    <td>{this.props.doctor.isActive ? 'Active' : 'InActive'}</td>
                </tr>
        );
    }
}

export default Doctor;