import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table } from 'reactstrap';

//components
import LocalDoctor from './Local-Doctor';

class LocalDoctorList extends Component {
    render() {
        let doctorsList = '';
        if(this.props.list.length) {
            doctorsList = this.props.list.map(each => {
                return <LocalDoctor key={Math.random()} data={each}/>
            });
        }
        return (
            <div>
                <Table>
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Location</th>
                        <th>Specility</th>
                        <th>Consult Fees</th>
                        <th>Consults</th>
                        <th>Schedule</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {doctorsList}
                    </tbody>
                </Table>
                {!doctorsList.length ? <div className='noList'>There is no newly Added Doctor</div>:null}
            </div>

        );
    }
}

const mapStateToProps = state => {
    console.log(state.reducer.doctors)
    return {
        list : state.reducer.doctors
    }
}

export default connect(mapStateToProps)(LocalDoctorList);