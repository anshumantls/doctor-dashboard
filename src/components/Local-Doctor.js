import React, { Component } from 'react';
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

//images
import EyeIcon from '../assets/eye.png';

class LocalDoctor extends Component {

    _setDayData = (array) => {
        const day = array.map((each, index) => {
            return  <span key={each._id}>
                        {each.from > 12 ? each.from-12 + 'PM' : each.from + 'AM'} to {each.to > 12 ? each.to - 12 + 'PM' : each.to + 'AM'}
                        {array[index+1] ? ' & ' : null}
                    </span>
        });
        return day;
    }
    
    render() {
        const {doctorDetails, avilability} = this.props.data;
        const {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday} = avilability
        let monday= '',tuesday='',wednesday='',thursday='',friday='',saturday='',sunday='';
        if(Monday.length) {
            monday = this._setDayData(Monday);
        }

        if(Tuesday.length) {
            tuesday = this._setDayData(Tuesday);
        }

        if(Wednesday.length) {
            wednesday = this._setDayData(Wednesday);
        }

        if(Thursday.length) {
            thursday = this._setDayData(Thursday);
        }

        if(Friday.length) {
            friday = this._setDayData(Friday);
        }

        if(Saturday.length) {
            saturday = this._setDayData(Saturday);
        }

        if(Sunday.length) {
            sunday = this._setDayData(Sunday);
        }

        return (
            <tr>
                <td>{doctorDetails.fullName}</td>
                    <td>{doctorDetails.email}</td>
                    <td>{doctorDetails.phone}</td>
                    <td>{doctorDetails.clinicOrHospitalName}</td>
                    <td>{doctorDetails.specialty ? doctorDetails.specialty : '-'}</td>
                    <td>{doctorDetails.fee ? doctorDetails.fee : '-'}</td>
                    <td>0</td>
                    <td>
                        <Button id={'lol_'+doctorDetails.id} type="button">
                            <img className='icon' alt='view' src={EyeIcon}/>
                        </Button>
                        <UncontrolledPopover trigger="focus" placement="bottom" target={'lol_'+doctorDetails.id}>
                            <PopoverHeader>Schedule</PopoverHeader>
                            <PopoverBody>
                                {monday.length ? <p>Monday: {monday}</p> : null}  
                                {tuesday.length ? <p>Tuesday: {tuesday}</p> : null}  
                                {wednesday.length ? <p>Wednesday: {wednesday}</p> : null}  
                                {thursday.length ? <p>Thursday: {thursday}</p> : null}  
                                {friday.length ? <p>Friday: {friday}</p> : null}  
                                {saturday.length ? <p>Saturday: {saturday}</p> : null}  
                                {sunday.length ? <p>Sunday: {sunday}</p> : null}                                
                            </PopoverBody>
                        </UncontrolledPopover>
                    </td>
                    <td>InActive</td>
            </tr>
        );
    }
}

export default LocalDoctor;