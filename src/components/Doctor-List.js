import React, { Component } from 'react';
import { makePostRequest } from '../http/http-service';
import { Table,Collapse, Navbar, NavbarBrand, Nav, Button } from 'reactstrap';
import Doctor from './Doctor';

class DoctorList extends Component {
    state = {
        pageNumber:1,
        pageSize:10,
        totalCount:0,
        doctors:[]
    }

    componentDidMount() {
        this._apiCall()
    }

    _apiCall = () => {
        console.log(this.state," apicall")
        const data = {
            pageNumber: this.state.pageNumber,
            pageSize: this.state.pageSize,
            filters: {}
        }
        makePostRequest('https://api-dev.askvaidya.in/admin/v1/doctors',true,data).then(data => this._setDoctorData(data));
    }

    _setDoctorData = (data) => {
        console.log(data)
        this.setState({
            doctors: data.doctors,
            totalCount: data.totalCount
        }, () => console.log(this.state))
    }

    _onChangeMaxResult = (value) => {
        console.log(value)
        this.setState({pageSize:Number(value),pageNumber:1}, () => this._apiCall());
    }

    _pageIncrementDecrement = (type) => {
        console.log(type);
        if(type === '-' && this.state.pageNumber > 1) {
            this.setState({pageNumber:this.state.pageNumber - 1},() => this._apiCall());
        }
        else if (type === '+' && this.state.totalCount >  this.state.pageSize*this.state.pageNumber) {
            this.setState({pageNumber:this.state.pageNumber + 1},() => this._apiCall());
        }
    }

    render () {
        let doctorsList = '' ;
        if (this.state.doctors.length) {
            doctorsList = this.state.doctors.map(doctor => {
                return  <Doctor key={doctor.id} doctor={doctor}/>
            })
        }
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Doctors</NavbarBrand>
                    <Collapse isOpen={true} navbar>
                    <Nav className="mr-auto" navbar>
                    </Nav>
                    <Button href='/add-doctor'>Add Doctor</Button>
                    </Collapse>
                </Navbar>
                <div>
                    <Table>
                        <thead>
                            <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Location</th>
                            <th>Specility</th>
                            <th>Consult Fees</th>
                            <th>Consults</th>
                            <th>Schedule</th>
                            <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {doctorsList}
                        </tbody>
                    </Table>
                </div>
                {!doctorsList.length ? <div className='noList'>There is no Doctor Data To Show</div>:
                <div className='pagination'>
                    <p>Showing Result {this.state.pageSize*this.state.pageNumber-(this.state.pageSize-1)} to {this.state.pageSize*this.state.pageNumber > this.state.totalCount ? this.state.totalCount : this.state.pageSize*this.state.pageNumber } out of {this.state.totalCount}</p>
                    <p>Show <select value={this.state.pageSize} onChange={(event) => this._onChangeMaxResult(event.target.value)}>
                            <option value='5'>5</option>
                            <option value='10'>10</option>
                            <option value='15'>15</option>
                            <option value='20'>20</option>
                        </select> Result Per Page</p>
                    <div></div>
                    <div>
                        <Button onClick={() => this._pageIncrementDecrement('-')}>-</Button>
                        {this.state.pageNumber}
                        <Button onClick={() => this._pageIncrementDecrement('+')}>+</Button>
                    </div>
                </div> }
            </div>
        );
    }
}

export default DoctorList;