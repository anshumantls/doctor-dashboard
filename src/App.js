import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom'; 

//components
import Navbar from './components/Navbar';
import DoctorList from './components/Doctor-List';
import AddDoctorForm from './components/Add-Doctor-Form';
import AddDoctorTiming from './components/Add-Doctor-Timing';
import LocalDoctorList from './components/Local-Doctor-List';
import NewLocalDoctor from './components/New-Local-Doctor';
import NewDoctorList from './components/New-Doctor-List';

class App extends Component {
  render () {
    return (
      <div className="App">
        <Navbar/>
        <Switch>
          <Route exact path='/doctor-list' component={DoctorList}/>
          <Route path='/add-doctor/step1' component={AddDoctorForm}/>
          <Route path='/add-doctor/step2' component={AddDoctorTiming}/>
          <Route path='/newly-added-doctors' component={LocalDoctorList}/>
          <Route path='/newly-added-doctors2' component={NewLocalDoctor} />
          <Route path='/doctor-list2' component={NewDoctorList}/>
          <Redirect from='/add-doctor' to='/add-doctor/step1'/>
          <Redirect from='/' to='doctor-list'/>
        </Switch>
      </div>
    );
  }
}

export default App;
